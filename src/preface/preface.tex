\newcommand{\aphorism}[3]{%
  \vspace{3ex plus 1ex minus .2ex}%
  \noindent\parbox{.85\textwidth}{\raggedright\itshape #1 \\ 
    \hspace*{\fill}{\upshape -- #2, \MakeUppercase{#3}}}}

\newcommand{\header}[1]{\vspace{1ex}\par\noindent\textbf{#1}\hspace{0.618em plus 0.1em minus 0.05em}}


\par\noindent{\Large\bfseries Preface}
\vspace{3ex}

The Theory of Computation is wonderful.
It is beautiful.
It has deep connections with other areas of computer science and
mathematics, as well as to the wider intellectual world.
And, looking forward into this century, 
clearly a theme will be the power of computation.
So it is timely also. 

It makes a delightful course.
Its organizing question\Dash what can be done?\Dash is both natural
and compelling.
Students see the contrast between computation's capabilities 
and limits.
There are well-understood principles 
and within easy reach are as-yet-unknown areas.

This text aims to reflect all of that:~to be precise,
topical, insightful, and perhaps sometimes even delightful.



% ------------------------
\header{For students}
We will study the ideas of computation more than the details.
We will be less interested in semicolons and cache hits than in
what things can be found with a mechanism at all.
This has been intensively studied for nearly a hundred years;
we will not see all that is known but we will see 
enough that you will end with an overview, with some
central insights.

Unifying it all is the computational perspective,
that there are organizing principles in
computation and that seeing how those principles hold across
different models gives a deep understanding of
both the problems and their solutions.

We do not stint on mathematics. 
But you will find the story told in a liberal way, in a way that, 
\citetext{in addition to technical detail, also aims for a breadth of knowledge}{%
  \protect\url{https://www.aacu.org/leap/what-is-a-liberal-education}}.
We will be eager to 
make connections with other fields and with
other modes of thinking.
We learn things more completely when they fit in a larger picture,
as the first quote below expresses.

The presentation here encourages you be an active learner, to
reflect on the motivation, development, and future of those ideas.
It also encourages you to follow up on things that you find interesting.
The back of the book has lots of notes to the main text,
which in turn contain lots of links to check out.
And, the Extra sections at the end of each chapter 
also help you explore.
Whether or not your instructor covers them formally in class,
these will further your understanding of the material
and of where it can lead.

The subject is big, and a challenge.
It is also a lot of fun.
Enjoy!


% ------------------------
\header{For instructors}
This text covers the standard topics:~the definition of
computability, unsolvability, 
languages, automata, and complexity.
The audience is undergraduate majors in computer science, mathematics, and
nearby areas.

The prerequisites are from the standard discrete mathematics
course:~propositional logic and truth tables,
proof methods including induction,
graphs,
basic number theory, 
sets, functions, and relations.
For some of these topics, such as graphs or big-$\bigOh$, 
there are optional review sections.
I also expect programming experience.

A text does students a disservice if its presentation is not
precise enough.
The details matter.
But students can also fail to understand the subject because they have
been overwhelmed with detail and have
not had a chance to reflect on the underlying ideas.
The presentation here aims
to give a fuller understanding
of the results both by placing the stress on motivation and naturalness over 
detail
and, where practical, by setting the results in a context.

An example of this difference comes at the
start in our choice of computing model, the Turing machine.
This is the traditional model, leads naturally 
to Finite State machines, and is standard in complexity.
Its downside is that it is awkward for extensive computation.
However, here we don't do extensive computations.
We immediately pass to a discussion of Church's Thesis, 
relying on the intuition that students have 
from their programming experience.
Besides giving some 
motivation and meaning to the formalities, this allows us to
sketch algorithms, which is both clearer and faster.

A second example is nondeterminism.
We introduce it in the context
of Finite State machines and pair that introduction with a discussion 
giving students a chance to reflect on this key idea.
Another example comes with the inclusion in the
complexity material of a section of the kind of problems that drive
the work today,
and still another
is the discussion of the current state of
$\P$ versus~$\NP$.
All of these examples, and many more, 
taken together
encourage students to reflect on the subject's larger ideas.






% ----------------------
\header{Schedule}
This is my semester's schedule. 
Chapter~I defines models of computation, Chapter~II covers unsolvability,
Chapter~III does languages, Chapter~IV does automata, and Chapter~V
is computational complexity.
\begin{center}
\begin{tabular}{r|lll}
  \multicolumn{1}{c}{\ }
             &\textit{Sections} &\textit{Reading} &\textit{Notes} \\ \cline{2-4}
  \textit{Week 1} &I.1, I.3, I.4      &I.2                       \\
  \textit{     2} &II.1, II.2         &II.3.1                    \\
  \textit{     3} &II.3, II.4         &II.B                      \\
  \textit{     4} &II.5, II.6         &                      \\
  \textit{     5} &II.8, III.1        &II.C                          \\
  \textit{     6} &III.2              &III.A      &\textsc{Exam} \\
  \textit{     7} &III.4, IV.1      &III.3                       \\
  \textit{     8} &IV.2, IV.3                                     \\
  \textit{     9} &IV.3, IV.4       &IV.A                        \\
  \textit{    10} &IV.5, IV.7       &                            \\
  \textit{    11} &V.1              &IV.1.4       &\textsc{Exam} \\
  \textit{    12} &V.2, V.3        &V.A                          \\
  \textit{    13} &V.4, V.5        &V.5.4                         \\
  \textit{    14} &V.5             &V.B          &\textsc{Review} \\
\end{tabular}
\end{center}
An instructor could omit the readings, except for the first,
but I give them as homework and quiz on them.



% ------------------------
\header{Speaking in code}
% Basing development on the prior work that students have done,
% tying new ideas to a learner's existing knowledge, are 
% sound teaching principles.
% And surely expressing our thoughts clearly and succinctly is also sound.
%
As the quote below by A~Downey expresses, for communicating
effective procedures, a
modern programming language is hard to beat as a
blend of precision and concision.
We use Scheme as a single, uniform, practical language for this. 
One reason is that it is very easy to learn.
Another is that it
\citetext{suits the topics}{John McCarthy, 
the inventor of Scheme's ancestor Lisp, suggested the language
as well-suited for the Theory of Computation.
Two arguments in that direction 
are the language's elegance and expressiveness.
This is captured  an online obituary of
McCarthy written by Bertrand Meyer for the 
\textit{Communications of the Association for Computing Machinery}:
``The Lisp 1.5 manual, published in 1962, was another masterpiece; 
as early as page 13 it introduces\Dash an unbelievable feat, especially 
considering that the program takes hardly more than half a page\Dash an 
interpreter for the language being defined, written in that very language! 
The more recent reader can only experience here the kind of visceral, 
poignant and inextinguishable jealously that overwhelms us the first time we 
realize that we will never be able to attend the premi\`ere of Don Giovanni at 
the Estates Theater in Prague on 29 October, 1787 \ldots\,. 
What may have been the reaction of someone in `Data Processing,' 
such as it was in 1962, suddenly coming across such a language manual?'' 
\autocite{MeyerB}}.
But the main reason is that, as E~Dijkstra says below, it is a delight.
% https://hardmath123.github.io/perchance-to-scheme.html


% -----------------------------
\header{Exploration, Enrichment, Connections}
This book encourages readers to be active and engaged.
First, it is written in lively language and
contains many illustrations,
from pictures of some of the people who have developed the topics
through to automata diagrams and graphs.
% This is not 1960; photos and flowcharts were at one time 
% expensive but not now.
The Theory of Computation is a stimulating subject
and it deserves a presentation to match.

The presentation also encourages active exploration through
the topics that end each chapter, which are both
fascinating and fun.
These are suitable as single day lectures, or to assign for group work,
or for extra credit, or just for looking at.

One way to stimulate readers to explore is with links.
There are many notes in the back that fill out, and perhaps add a spark to, the 
core presentation.
Where practical, the references are clickable.
For example, the pictures of the people who discovered the subject
are a link to their Wikipedia page.
I hope readers enjoy these and I know that because it is linked-to,
this is very much more likely to be
seen than is the same content in a library.




%---------------------------------------
\header{License}
This book is Free.
You can use it without cost. 
You can also redistribute it\Dash
an instructor can make copies and distribute it through their bookstore
or as a file on their school's intranet.
You can also get the \LaTeX{} source
and modify it to suit your class;
see \url{http://joshua.smcvt.edu/computation}. 

One reason that it is Free is that it is written in 
\LaTeX{}, which is Free, as is our Scheme implementation,
as is Asymptote that drew the illustrations,
as is Emacs and all of GNU software and indeed
the entire Linux platform on which this book was
developed.
And besides those, all of the research that this text
presents was made freely available by scholars.
I believe that the synthesis here adds
value\Dash I hope so, indeed\Dash but the masters have left a well-marked
path and following it seems only right.


%---------------------------------------
\header{Acknowledgements}
I owe a great debt to my wife, whose patience with this
project has gone beyond all reasonable limits.
My students have also been patient and have made the book better in many ways.

And, I must acknowledge my teachers.
There are many but first among them is M~Lerman.
Thank you, Manny.

They also include H~Abelson, GJ~Sussmann, and J~Sussmann, who
had the courage with
\textit{Structure  and Interpretation of Computer Programs} to show
students just how mind-blowing it all is.
When I see an introduction to programming
where the examples are about managing
inventory in a used car dealership I can only say:~Thank you,
for believing in me.


\vspace{1ex plus .25ex minus .1ex}
\aphorism{Memory works far better when you learn networks of facts rather than facts in isolation.}{T~Gowers}{WHAT MATHS A-LEVEL DOESN'T NECESSARILY GIVE YOU} 
% https://gowers.wordpress.com/2012/11/20/what-maths-a-level-doesnt-necessarily-give-you/

\vspace{1ex plus .25ex minus .1ex}
\aphorism{Teach concepts, not tricks.}{Gian-Carlo Rota}{TEN LESSONS I WISH I HAD LEARNED BEFORE I STARTED TEACHING
DIFFERENTIAL EQUATIONS}
% from an MAA talk

\vspace{1ex plus .25ex minus .1ex}
\aphorism{[W]hile many distinguished scholars have embraced
[the Jane Austen Society]
and its delights since the founding meeting, ready to don period dress,
eager to explore antiquarian minutiae, and happy to stand up at the
Saturday-evening ball, 
others, in their studies of Jane Austen's works,
\ldots{} have described how, as professional scholars, they are
rendered uneasy by this performance of \emph{pleasure} at
[the meetings].
\ldots\@
I am not going to be one of those scholars.
}{Elaine Bander}{Persuasions, 2017}
% from p 151


\vspace{1ex plus .25ex minus .1ex}
\aphorism{The power of modern programming languages is that they are expressive, readable, concise, precise, and executable. That means we can eliminate middleman languages and use one language to explore, learn, teach, and think.}{A Downey}{Programming as a Way of Thinking}
% from https://blogs.scientificamerican.com/guest-blog/programming-as-a-way-of-thinking/nn, April 26, 2017


\vspace{1ex plus .25ex minus .1ex}
\aphorism{Lisp has jokingly been called ``the most intelligent way to misuse a computer.'' I think that description is a great compliment because it transmits the full flavor of liberation: it has assisted a number of our most gifted fellow humans in thinking previously impossible thoughts.}{E Dijkstra}{CACM, 15:10}



\vspace{1ex plus .25ex minus .1ex}
\aphorism{Of what use are computers?  They can only give answers.}{P Picasso}{The Paris Review, Summer-Fall 1964}

% The only difference with these notes is that … well, they cover exactly the topics I’d cover, in exactly the order I’d cover them, and with exactly the stupid jokes and stories I’d tell in a given situation. 
% Scott Aaronson https://www.scottaaronson.com/blog/?p=3943

\vspace{2ex plus 1\fill}
\begin{flushright}
  \begin{tabular}{l@{}}
    Jim Hef{}feron \\ 
    Saint Michael's College \\
    Colchester, VT USA \\
    \texttt{joshua.smcvt.edu/computing} \\
    Draft: version 0.86 %, \ymd{2018}{12}{25}.
  \end{tabular}
\end{flushright}

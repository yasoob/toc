// flowcharts.asy
//  Flow charts illustrating routines
cd("../../../asy");
import settexpreamble;
cd("");
settexpreamble();

cd("../../../asy");
import jh;
import flowchart;
cd("");

import settings;
settings.outformat="pdf";

cd("../../../asy/asy-graphtheory-master/modules");
import node;
cd("");

string OUTPUT_FN = "flowcharts%02d";



// ---- s-m-n theorem
picture pic;
int picnum = 0;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e,x_0,\ldots,x_{m-1}$");
node create=nbox("Create instructions for $\hat{P}$");
node ret=nbox(minipage_snug("Return index of\\ that instruction set"));
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1.0*v,start,read);
vlayout(1.0*v,read,create);
vlayout(1.275*v,create,ret);
vlayout(1.25*v,ret,ending);

// draw edges
draw(pic,
     (start--read),
     (read--create),
     (create--ret),
     (ret--ending)
);

// draw nodes
draw(pic,
     start,
     read,
     create,
     ret,
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ---------------------------
picture pic;
int picnum = 1;

// define nodes
node start=nroundbox("Start");
node shiftleft=nbox("Move left $x_0+\cdots+x_{m-1}+m$ cells");
node inputprefix=nbox(minipage_snug("Put $x_0$, \ldots, $x_{m-1}$ on tape\\ separated by blanks"));
node pointer=nbox("Move I/O head to start of $x_0$");
node run=nbox("Simulate $P_e$");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1.0*v,start,shiftleft);
vlayout(1.25*v,shiftleft,inputprefix);
vlayout(1.275*v,inputprefix,pointer);
vlayout(1.0*v,pointer,run);
vlayout(1.0*v,run,ending);

// draw edges
draw(pic,
     (start--shiftleft),
     (shiftleft--inputprefix),
     (inputprefix--pointer),
     (pointer--run),
     (run--ending)
);

// draw nodes
draw(pic,
     start,
     shiftleft,
     inputprefix,
     pointer,
     run,
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================= Proof of HP ==============
picture pic;
int picnum = 2;


// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e$");
node test=nrounddiamond("$K(e)=1$?");
node printout=nbox("Print 0");
node loop=nbox("Infinite loop");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1.35*v,read,test);
hlayout(-3*u,test,printout);
hlayout(3.35*u,test,loop);
vlayout(1.1*v,printout,ending);

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout).l("No"),
     (test--loop).l("Yes").style("leftside"),
     (printout--ending)
);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     loop, 
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ---- relativization to K -------------
picture pic;
int picnum = 3;


// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e$");
node test=nrounddiamond("$\TMfcn^K_e(e)\converges$?");
node printout=nbox("Print 0");
node loop=nbox("Infinite loop");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1.35*v,read,test);
hlayout(-2.8*u,test,printout);
hlayout(3.15*u,test,loop);
vlayout(1.1*v,printout,ending);

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout).l("No"),
     (test--loop).l("Yes").style("leftside"),
     (printout--ending)
);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     loop, 
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ================= K\leq K^K ==============
picture pic;
int picnum = 4;


// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x,y$");
node test=nrounddiamond("oracle(x)?");
node printzero=nbox("Print 0");
node loop=nbox("Loop");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1.35*v,read,test);
printzero.pos = test.pos + (-2*u,-1.0*v);
loop.pos = test.pos + (2*u,-1.0*v);
vlayout(1.0*v,printzero,ending);

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test..HV..printzero).l("Y"),
     (test..HV..loop).l("N").style("leftside"),
     (printzero--ending)
);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printzero,
     loop, 
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........ apply s-m-n thm ........
picture pic;
int picnum = 5;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node test=nrounddiamond("oracle(x)?");
node printzero=nbox("Print 0");
node loop=nbox("Loop");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1.35*v,read,test);
printzero.pos = test.pos + (-2*u,-1.0*v);
loop.pos = test.pos + (2*u,-1.0*v);
vlayout(1.0*v,printzero,ending);

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test..HV..printzero).l("Y"),
     (test..HV..loop).l("N").style("leftside"),
     (printzero--ending)
);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printzero,
     loop, 
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// // ---- relativization to K -------------
// picture pic;
// int picnum = 5;

// // define nodes
// node start=nroundbox("Start");
// node read=nbox("Read $y$");
// node test=nrounddiamond("oracle(x)?");
// node printzero=nbox("Print 0");
// node printone=nbox("Print 1");
// node ending=nroundbox("End");

// // layout
// defaultlayoutrel = false;
// defaultlayoutskip = 0.75cm;
// real u = defaultlayoutskip;
// real v = 0.85*u;

// vlayout(1*v,start,read);
// vlayout(1.35*v,read,test);
// vlayout(1.3*v,test,ending);
// hlayout(-2.0*u,ending,printzero);
// hlayout(2.0*u,ending,printone);

// // draw edges
// draw(pic,
//      (start--read),
//      (read--test),
//      (test..HV..printzero).l("N"),
//      (test..HV..printone).l("Y").style("leftside"),
//      (printzero--ending),
//      (printone--ending)
// );

// // draw nodes
// draw(pic,
//      start,
//      read, 
//      test,
//      printzero,
//      printone, 
//      ending
//      );

// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ---- recursion theorem -------------
picture pic;
int picnum = 6;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $n,x$");
node getindex=nbox("Run $\TM_n$ on $n$");
node simulate=nbox(minipage_snug("With the result $w$,\\run $\TM_w$ on input $x$"));
node end=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1*v,read,getindex);
vlayout(1.4*v,getindex,simulate);
vlayout(1.3*v,simulate,end);

// draw edges
draw(pic,
     (start--read),
     (read--getindex),
     (getindex--simulate),
     (simulate--end)
     );

// draw nodes
draw(pic,
     start,
     read, 
     getindex,
     simulate, 
     end
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...................
picture pic;
int picnum = 7;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x$");
node getindex=nbox("Run $\TM_n$ on $n$");
node simulate=nbox(minipage_snug("With the result $w$,\\run $\TM_w$ on input $x$"));
node end=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1*v,read,getindex);
vlayout(1.4*v,getindex,simulate);
vlayout(1.3*v,simulate,end);

// draw edges
draw(pic,
     (start--read),
     (read--getindex),
     (getindex--simulate),
     (simulate--end)
     );

// draw nodes
draw(pic,
     start,
     read, 
     getindex,
     simulate, 
     end
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...................
picture pic;
int picnum = 8;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x$");
node getindex=nbox("Run $\TM_n$ on $n$");
node simulate=nbox("With the result $w$, run $\TM_{f(w)}$ on $x$");
node end=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1*v,read,getindex);
vlayout(1.2*v,getindex,simulate);
vlayout(1.2*v,simulate,end);

// draw edges
draw(pic,
     (start--read),
     (read--getindex),
     (getindex--simulate),
     (simulate--end)
     );

// draw nodes
draw(pic,
     start,
     read, 
     getindex,
     simulate, 
     end
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ---- application of recursion theorem W_m={m}  -------------
picture pic;
int picnum = 9;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x,m$");
node test=nrounddiamond("x=m?");
node printzero=nbox("Print 0");
node loop=nbox("Loop");
node dummy=nbox("");  
node end=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1.35*v,read,test);
vlayout(0.9*v,test,dummy);
hlayout(-1.5*u,dummy,printzero);
vlayout(1.0*v,printzero,end);
hlayout(1.5*u,dummy,loop);

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test..HV..printzero).l("Y"),
     (test..HV..loop).l("N").style("leftside"),
     (printzero--end)
);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printzero,
     loop, 
     end
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..........................
picture pic;
int picnum = 10;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x$");
node test=nrounddiamond("x=m?");
node printzero=nbox("Print 0");
node loop=nbox("Loop");
node dummy=nbox("");  
node end=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1*v,start,read);
vlayout(1.35*v,read,test);
vlayout(0.9*v,test,dummy);
hlayout(-1.5*u,dummy,printzero);
vlayout(1.0*v,printzero,end);
hlayout(1.5*u,dummy,loop);

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test..HV..printzero).l("Y"),
     (test..HV..loop).l("N").style("leftside"),
     (printzero--end)
);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printzero,
     loop, 
     end
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ---- Exercise on Fixed Point Theorem
picture pic;
int picnum = 11;

// define nodes
node start=nroundbox("Start");
node pstart=nbox("Print ``Start''");
node pprinte=nbox("Print ``Print ''+ $e$");
node pend=nbox("Print ``End''");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1.0*v,start,pstart);
vlayout(1.0*v,pstart,pprinte);
vlayout(1.0*v,pprinte,pend);
vlayout(1.0*v,pend,ending);

// draw edges
draw(pic,
     (start--pstart),
     (pstart--pprinte),
     (pprinte--pend),
     (pend--ending)
);

// draw nodes
draw(pic,
     start,
     pstart,
     pprinte,
     pend,
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........... second part of that exercise
picture pic;
int picnum = 12;

// define nodes
node start=nroundbox("Start");
node ppofe=nbox("Print machine number P(e)");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;
real v = 0.85*u;

vlayout(1.0*v,start,ppofe);
vlayout(1.0*v,ppofe,ending);

// draw edges
draw(pic,
     (start--ppofe),
     (ppofe--ending)
);

// draw nodes
draw(pic,
     start,
     ppofe,
     ending
     );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


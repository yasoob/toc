// perfect.asy
//  Flow charts showing Halting Problem args
cd("../../../asy");
import settexpreamble;
cd("");
settexpreamble();

cd("../../../asy");
import jh;
import flowchart;
cd("");

import settings;
settings.outformat="pdf";

cd("../../../asy/asy-graphtheory-master/modules");
import node;
cd("");
// size(0.5cm);  // units are big points: 72 is 1inch

// pen NODEPEN=fontsize(8pt);
// pen EDGEPEN=fontsize(6pt); // +fontcommand("\ttfamily");
// // // define edge style
// defaultdrawstyle=drawstyle(p=EDGEPEN+fontcommand("\sffamily"), arrow=Arrow(DefaultHead,size=3));
// // // Standard node is single-circle border
// defaultnodestyle=nodestyle(textpen=NODEPEN+fontcommand("\sffamily"), xmargin=1pt, drawfn=FillDrawer(backgroundcolor,black));
// // // Double circle nodes
// nodestyle ns_accepting=nodestyle(textpen=NODEPEN+fontcommand("\sffamily"), drawfn=Filler(FILLCOLOR)+DoubleDrawer(black));
// // // nodes without any boxing
// nodestyle ns_noborder=nodestyle(textpen=NODEPEN+fontcommand("\sffamily"), xmargin=1pt, drawfn=None);

// ---- Unbounded search for perfect numbers
picture pic;
int picnum = 0;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x$");
node initial=nbox("$i=0$");
node iterate=nbox("$i=i+1$");
node test=nrounddiamond("$2i+1$ perfect?");
node printout=nbox("Print 1");
node loop=nbox("Infinite loop");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,initial);
vlayout(1.5u,initial,test);
// hlayout(-3.75u,test,printout);
vlayout(1.5u,test,printout);
hlayout(-3.75u,test,iterate);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read,
     initial,
     test,
     iterate,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--initial),
     (initial--test),
     (iterate--VH--middle(initial,test)),
     (test--printout).l("Yes").style("leftside"),
     (test--iterate).l("No").style("leftside"),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- With halt_checker all fcns total
picture pic;
int picnum = 1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e,x$");
node test=nrounddiamond("$\phi_e(x)\!\converges$\,?");
node printout=nbox("Print $\phi_e(x)$");
node ow=nbox("Print $0$");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(1.25u,read,test);
// hlayout(-3.75u,test,printout);
vlayout(1.5u,test,printout);
hlayout(-2.5u,printout,ow);
vlayout(1.25u,printout,ending);

// draw nodes
draw(pic,
     start,
     read,
     test,
     printout,
     ow,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--HV--ow).label(Label("No",position=Relative(0.25))).style("rightside"),
     (test--printout).l("Yes").style("leftside"),
     (ow--VH--middle(printout,ending)),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");




// ---- Diagonalization to get a contradiction
picture pic;
int picnum = 2;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e$");
node simulate=nbox(minipage_snug("Compute table entry\\for index~$e$, input~$e$")); // was minipage3
node printout=nbox("Print $\text{result}+1$");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(start, read);
vlayout(1.25u,read,simulate);
vlayout(1.25u,simulate,printout);
vlayout(printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     simulate,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--simulate),
     (simulate--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");




// ---- K0 reduces to K, before s-m-n thm
picture pic;
int picnum = 3;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e,x,y$");
node simulate=nbox(minipage2("Simulate $\TM_{e}$\\ on input~$x$"));
node printout=nbox("Output 0");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(start,read);
vlayout(1.25u,read,simulate);
vlayout(1.25u,simulate,printout);
vlayout(printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     simulate,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--simulate),
     (simulate--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");


// ---- K0 reduces to K, after s-m-n thm
picture pic;
int picnum = 4;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node simulate=nbox(minipage2("Simulate $\TM_{e}$\\ on input~$x$"));
node printout=nbox("Output 0");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(start,read);
vlayout(1.25u,read,simulate);
vlayout(1.25u,simulate,printout);
vlayout(printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     simulate,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--simulate),
     (simulate--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- the Halting Problem is unsolvable
picture pic;
int picnum = 5;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e$");
node test=nrounddiamond("$\TM_e(e)$ halts?");
node printout=nbox("Print 0");
node loop=nbox("Infinite loop");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(1.2u,read,test);
hlayout(-3.5u,test,printout);
hlayout(3.85u,test,loop);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     loop, 
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout).l("No"),
     (test--loop).l("Yes").style("leftside"),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- halts on three is unsolvable, before s-m-n
picture pic;
picnum = 6;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x,y$");
node test=nbox("Run $\TM_x$ on~$x$");
node printout=nbox("Print 42");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- halts on three is unsolvable, after s-m-n
picture pic;
picnum = 7;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node test=nbox("Run $\TM_x$ on~$x$");
node printout=nbox("Print 42");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");


// ---- outputs seven is unsolvable, before s-m-n
picture pic;
picnum = 8;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x,y$");
node test=nbox("Run $\TM_x$ on~$x$");
node printout=nbox("Print 7");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- outputs seven is unsolvable, after s-m-n
picture pic;
picnum = picnum+1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node test=nbox("Run $\TM_x$ on~$x$");
node printout=nbox("Print 7");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- doubler is unsolvable, before s-m-n
picture pic;
picnum = picnum+1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x,y$");
node test=nbox("Run $\TM_x$ on~$x$");
node printout=nbox("Print 2y");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- doubler is unsolvable, after s-m-n
picture pic;
picnum = picnum+1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node test=nbox("Run $\TM_x$ on~$x$");
node printout=nbox("Print 2y");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- Rice's Thm, before s-m-n
picture pic;
picnum = picnum+1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $x,y$");
node sim1=nbox("Run $\TM_x$ on~$x$");
node sim2=nbox("Run $\TM_{e_1}$ on~$y$");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,sim1);
vlayout(0.85u,sim1,sim2);
vlayout(0.85u,sim2,ending);

// draw nodes
draw(pic,
     start,
     read, 
     sim1,
     sim2,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--sim1),
     (sim1--sim2),
     (sim2--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- Rice's Thm, after s-m-n
picture pic;
picnum = picnum+1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node sim1=nbox("Run $\TM_x$ on~$x$");
node sim2=nbox("Run $\TM_{e_1}$ on~$y$");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,sim1);
vlayout(0.85u,sim1,sim2);
vlayout(0.85u,sim2,ending);

// draw nodes
draw(pic,
     start,
     read, 
     sim1,
     sim2,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--sim1),
     (sim1--sim2),
     (sim2--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- operating systems
picture pic;
picnum = picnum+1;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $e,x$");
node sim=nbox(minipage("\centering Simulate $\TM_e$ on input~$x$",1.85cm));
node print=nbox("Print result");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(1.15u,read,sim);
vlayout(1.15u,sim,print);
vlayout(0.85u,print,ending);

// draw nodes
draw(pic,
     start,
     read, 
     sim,
     print,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--sim),
     (sim--print),
     (print--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");



// ---- halts on three is unsolvable, family of functions
picture pic;
picnum = 15;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node test=nbox("Run $\TM_0$ on~$0$");
node printout=nbox("Print 42");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");


// ...................................
picture pic;
picnum = 16;

// define nodes
node start=nroundbox("Start");
node read=nbox("Read $y$");
node test=nbox("Run $\TM_1$ on~$1$");
node printout=nbox("Print 42");
node ending=nroundbox("End");

// layout
defaultlayoutrel = false;
defaultlayoutskip = 0.75cm;
real u = defaultlayoutskip;

vlayout(0.85u,start,read);
vlayout(0.85u,read,test);
vlayout(0.85u,test,printout);
vlayout(0.85u,printout,ending);

// draw nodes
draw(pic,
     start,
     read, 
     test,
     printout,
     ending
     );

// draw edges
draw(pic,
     (start--read),
     (read--test),
     (test--printout),
     (printout--ending)
);

shipout(format("hp%02d",picnum),pic,format="pdf");

